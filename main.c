/**
 * @file main.c
 * @author jan de rudder (jan.de_rudder@etudian.univ-lr.fr)
 * @date 2022-11-25
 *
 * @brief Testing the various ways to hide a function from outside reach,
 * and the effects of declaring a function static in a header (which seems to
 * work without problems).
 * You may compile this sample project with the following command:
 *
 * $CC -std=c17 -I. -o bin/mytype-main ./MyType.c ./main.c
 *
 */
#include <stdio.h>

#include "MyType.h"


/* extern declaration of a private function suppressing a warning 
   if called further below, but not necessary for linking. */
// MyType* _mytype_multiply_by_hundred(MyType* mytype);


int main(void)
{
    MyType* mytype = mytype_create();

    /*
        Linker Error

        The function being static it is unreachable so it is undefined.

        Comment out this line to be able to compile.
    */
    // _mytype_inc(mytype);

    /*
        Compiler Warning

        This function not being 'static', it is reachable from outside the
        file where it is declared.
        But the compiler throws a warning saying it is implicitely decalared,
        which in not valid C (starting with C99).

        Comment out this line to suppress the warning.
    */
    _mytype_dec(mytype);

    /*
        Linker Error

        This function, declared in its .c file, being static, is unreachable
        from here.
    */
    // _mytype_multiply_by_ten(mytype);

    /*
        Compiler Warning: implicit declaration.

        This warning can be suppressed by making an extern declaration for
        this function just before our main function.
    */
    _mytype_multiply_by_hundred(mytype);

    printf("mytype->i is %d\n", getI(mytype));

    return 0;
}
