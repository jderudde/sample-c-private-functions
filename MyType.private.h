#ifndef MYTYPE_PRIVATE_H_
#define MYTYPE_PRIVATE_H_


typedef struct _MyType
{
    int i;
} _MyType;


// private functions
static _MyType* _mytype_inc(_MyType* mytype);
_MyType* _mytype_dec(_MyType* mytype);

// there are yet two private functions that are not declared here



#endif // MYTYPE_PRIVATE_H_
