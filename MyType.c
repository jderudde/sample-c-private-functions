#include "MyType.h"
#include "MyType.private.h"

#include <stdlib.h>



////////////////////////////////////////////////////////////////////////////////
// private functions
////////////////////////////////////////////////////////////////////////////////
static _MyType* _mytype_inc(_MyType* mytype)
{
    mytype->i += 1;
    return mytype;
}



_MyType* _mytype_dec(_MyType* mytype)
{
    mytype->i -= 1;
    return mytype;
}



static _MyType* _mytype_multiply_by_ten(_MyType* mytype)
{
    mytype->i *= 10;
    return mytype;
}



_MyType* _mytype_multiply_by_hundred(_MyType* mytype)
{
    mytype->i *= 100;
    return mytype;
}



////////////////////////////////////////////////////////////////////////////////
// public functions
////////////////////////////////////////////////////////////////////////////////
MyType* mytype_create(void)
{
    MyType* mytype = malloc(sizeof(MyType));
    mytype->i = 1;
    return mytype;
}



int getI(const MyType* mytype)
{
    return mytype->i;
}



int getIncrementedI(MyType* mytype)
{
    return _mytype_inc(mytype)->i;
}
