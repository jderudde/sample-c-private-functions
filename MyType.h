#ifndef MYTYPE_H_
#define MYTYPE_H_

typedef struct _MyType MyType;

MyType* mytype_create(void);

int getI(const MyType* mytype);
int getIncrementedI(MyType* mytype);

#endif // MYTYPE_H_
